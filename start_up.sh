#!/bin/bash

# Define the date format for filenames
current_date=$(date '+%Y-%m-%d')

# Define the Python script to run
python_script="run.py"

# Define directories and file names
log_dir="logs"
log_file="${log_dir}/Logs-${current_date}.txt"
backup_dir="output/Backup/${current_date}"

# Ensure the log directory exists
mkdir -p "${log_dir}"

# Run the Python script unbuffered and save the output to the log file, displaying it in real-time
echo "Running ${python_script}..."
python3 -u "${python_script}" 2>&1 | stdbuf -oL tee "${log_file}"
echo "Output has been saved to ${log_file}"

# Ensure the backup directory exists
mkdir -p "${backup_dir}"

# Copy the XLSX files to the backup directory
echo "Backing up XLSX files..."
cp output/*.xlsx "${backup_dir}/"

echo "Backup completed. Files are stored in ${backup_dir}/"
