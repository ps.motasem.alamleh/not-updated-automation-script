# Ticket Reminder System

This repository contains a script that automates the process of checking ticket statuses and sending reminders based on certain conditions.

## Overview

The script navigates through a list of ticket links, scrapes relevant data, and then determines the appropriate reminder or escalation status for each ticket based on the scraped data.

### Key Features:

1. **Automated Web Scraping**: The script uses web automation to navigate through ticket links and scrape required data.
2. **Dynamic Reminder System**: Depending on the ticket's due date, last update, and type, the script determines the appropriate reminder or escalation status.
3. **Support for Multiple Ticket Types**: The script can handle different types of tickets, such as "Ticket" and "Service", and apply different reminder rules for each.

## How It Works

1. **Data Scraping**: The script navigates to each ticket link, waits for the page to load, and then scrapes relevant data like due date, last updated date, ticket type, and the first comment.
2. **Reminder Logic**: For each ticket, the script checks:
   - If a "third reminder" is mentioned in the first comment, it checks for the conditions to escalate.
   - If a "second reminder" is mentioned, it checks for conditions to send a third reminder.
   - If the due date is in the past or not available, it sends a short reminder based on the ticket type and last updated date.

## Requirements

- Python 3.x
- Selenium
- Web driver (e.g., ChromeDriver for Chrome)

## Usage

1. Clone the repository.
2. Install the required packages.
3. Update the `ticket_links` list with the links to the tickets you want to check.
4. Run the script.

## Future Enhancements

- Integration with email or messaging services to send reminders automatically.
- Adding a GUI for easier use.
