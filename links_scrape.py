from selenium.webdriver.common.by import By
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import openpyxl
import time
import os
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException


# Start a browser session
chrome_options = webdriver.ChromeOptions()
current_directory = os.getcwd()
cookies_path = os.path.join(current_directory, "chrome-cookies")
chrome_options.add_argument(f"--user-data-dir={cookies_path}")


browser = webdriver.Chrome(options=chrome_options)
browser.get('https://support.progressoft.com/dash/home')

# def is_logged_in(driver):
#     """Check if we're already logged in."""
#     try:
#         driver.find_element(By.CSS_SELECTOR, 'body > div.headerWrapper > app-dashboard-header > div > div > table > tr > td.issueBtnContainer > button')
#         return True
#     except NoSuchElementException:
#         return False


# # Navigate to the portal's main page
# browser.get('https://support.progressoft.com/dash/home')

# # This code for login in to the portal make sure to set up the cookies after running it.

# if not is_logged_in(browser):
#     # Navigate to the login page if not already logged in
#     browser.get('https://support.progressoft.com/dash/home')

    
#     main_window_handle = browser.current_window_handle
    
#     for handle in browser.window_handles:
#         if handle != main_window_handle:
#             browser.switch_to.window(handle)
#             break

#     time.sleep(3)

#     email_elem = browser.find_element(By.CSS_SELECTOR, '#i0116')
#     email_elem.send_keys('YOUR_EMAIL')
#     ms_next_btn = browser.find_element(By.CSS_SELECTOR, '#idSIButton9')
#     ms_next_btn.click()

#     time.sleep(3)

#     password_elem = browser.find_element(By.CSS_SELECTOR,'#i0118')
#     password_elem.send_keys('YOUR_PASSWORD')
#     password_elem.submit()

# print ("Loged in")

# time.sleep(3)

issue_key_and_links = []

# Initialize Excel workbook and worksheet
wb = openpyxl.Workbook()
ws = wb.active

while True:

    wait = WebDriverWait(browser, 15)
    wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, '#cdk-drop-list-0 > div:nth-child(3) > div > div.myBox.scroller.ng-star-inserted > div > div > app-display-filter-results > div > div > table > tbody > tr')))

    ticket_rows = browser.find_elements(By.CSS_SELECTOR, '#cdk-drop-list-0 > div:nth-child(3) > div > div.myBox.scroller.ng-star-inserted > div > div > app-display-filter-results > div > div > table > tbody > tr')

    
    last_ticket_current_page = ticket_rows[-1].text

    for row in ticket_rows:
        try:

            # Extract only the issue key and link
            issue_key = row.find_element(By.CSS_SELECTOR, 'td.mat-cell.cdk-cell.basedWidth.cdk-column-key.mat-column-key.ng-star-inserted').text
            link_element = row.find_element(By.TAG_NAME, 'a')
            link = link_element.get_attribute('href')

            ticket_type = row.find_element(By.CSS_SELECTOR, 'td.mat-cell.cdk-cell.basedWidth.cdk-column-issue_type.mat-column-issue_type.ng-star-inserted').text
            if ticket_type == "Change Request":
                continue


            # Save the issue key and link
            issue_key_and_links.append((issue_key, link))
            
        except Exception as e:
            print(f"Error processing a row: {e}")


    try:
        next_button = browser.find_element(By.CSS_SELECTOR, '#cdk-drop-list-0 > div:nth-child(3) > div > div.myBox.scroller.ng-star-inserted > div > div > app-display-filter-results > div > div > mat-paginator > div > div > div.mat-paginator-range-actions > button.mat-focus-indicator.mat-tooltip-trigger.mat-paginator-navigation-next.mat-icon-button.mat-button-base > span.mat-button-wrapper > svg')
        if not next_button.is_enabled():
            break
        
        next_button.click()
        time.sleep(2)  
        
        ticket_rows_new = browser.find_elements(By.CSS_SELECTOR, '#cdk-drop-list-0 > div:nth-child(3) > div > div.myBox.scroller.ng-star-inserted > div > div > app-display-filter-results > div > div > table > tbody > tr')
        last_ticket_new_page = ticket_rows_new[-1].text
        
        if last_ticket_current_page == last_ticket_new_page:
            break
        
    except NoSuchElementException:
        break

# Save to Excel
headers = ["Issue Key", "Link"]
for col_num, header in enumerate(headers, 1):
    ws.cell(row=1, column=col_num, value=header)

# Add issue keys and links to the Excel sheet
for row_num, (issue_key, link) in enumerate(issue_key_and_links, 2):  # Starting from row 2
    ws.cell(row=row_num, column=1, value=issue_key)
    ws.cell(row=row_num, column=2, value=link)

wb.save('output/tickets.xlsx')

browser.quit()
