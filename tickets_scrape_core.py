from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from datetime import datetime, timedelta
import smtplib
from email.message import EmailMessage
import pandas as pd
import openpyxl
import re
import time
import os



# def get_assignee_and_team_details(team_name, uid):
   
# # List of possible suffixes for team files that follow the pattern "BaseName-Suffix.xlsx"
#     suffixes = ["Implementation", "Support"]
#     # Path for a directly named file, like "Payment Ops.xlsx"
#     direct_file_path = f"Teams-Emails/{team_name}.xlsx"

#     # First, check if a direct file exists for the base team name
#     if os.path.exists(direct_file_path):
#         file_paths = [direct_file_path]  # Only search in the direct file if it exists
#     else:
#         # If no direct file, prepare to check files with suffixes
#         file_paths = [f"Teams-Emails/{team_name}-{suffix}.xlsx" for suffix in suffixes]


#     for team_file_path in file_paths:
#         if not os.path.exists(team_file_path):
#             print(f"File {team_file_path} not found. Checking next file.")
#             continue
        
#         try:
#             df_team = pd.read_excel(team_file_path, engine='openpyxl')
#         except Exception as e:  # Broad exception catch for any error during file reading
#             print(f"Error reading {team_file_path}: {e}. Checking next file.")
#             continue

    
#     try:
#         df_team = pd.read_excel(team_file_path, engine='openpyxl')
#     except FileNotFoundError:
#         return {"error": f"File {team_file_path} not found."}
    
#     matching_rows = df_team[df_team['UID'].str.replace(" ", "").str.lower() == uid.replace(" ", "").lower()]
  
    
#     if not matching_rows.empty:
#         assignee_email = matching_rows['Email'].iloc[0]
#         assignee_name = matching_rows['Team Members'].iloc[0]
#     else:
#         return {"error": f"UID {uid} not found for team {team_name}."}
    
#     df_teams = pd.read_excel("Teams-Emails/Teams Info.xlsx", engine='openpyxl')
#     team_email_row = df_teams[df_teams['Team Name'].str.replace(" ", "") == team_name.replace(" ", "")]

#     if not team_email_row.empty:
#         team_email = team_email_row['Team Email'].iloc[0]
#     else:
#         return {"error": f"Team {team_name} not found in the Teams-Info.xlsx file."}
    
#     details = {
#         "assignee_email": assignee_email,
#         "assignee_name": assignee_name,
#         "team_email": team_email
#     }
#     print(f"Team Name: {team_name}")
#     print(f"UID: {uid}")
#     print(f"Assignee Name: {details['assignee_name']}")
#     print(f"Assignee Email: {details['assignee_email']}")
#     print(f"Team Email: {details['team_email']}")

#     return details
    
def get_assignee_and_team_details(base_team_name, uid):
    # List of possible suffixes for team files that follow the pattern "BaseName-Suffix.xlsx"
    suffixes = ["Support", "Implementation"]
    direct_file_path = f"Teams-Emails/{base_team_name}.xlsx"
    
    team_email = None
    found_file_name = None
    assignee_email = None
    assignee_name = None
    
    # First, try to find a direct match for the base team name
    if os.path.exists(direct_file_path):
        file_paths = [direct_file_path]
    else:
        # Prepare to search in files with suffixes if no direct match is found
        file_paths = [f"Teams-Emails/{base_team_name} {suffix}.xlsx" for suffix in suffixes]

    for team_file_path in file_paths:
        if not os.path.exists(team_file_path):
            print(f"File {team_file_path} not found. Checking next file.")
            continue
        
        try:
            df_team = pd.read_excel(team_file_path, engine='openpyxl')
        except Exception as e:
            print(f"Error reading {team_file_path}: {e}. Checking next file.")
            continue

        matching_rows = df_team[df_team['UID'].str.replace(" ", "").str.lower() == uid.replace(" ", "").lower()]
        
        if not matching_rows.empty:
            assignee_email = matching_rows['Email'].iloc[0]
            assignee_name = matching_rows['Team Members'].iloc[0]
            found_file_name = os.path.basename(team_file_path)  # Extracts the file name from the path
            break

    if assignee_email is None:
        # If no matching UID is found in any of the files
        return None,{"error": f"UID {uid} not found for team {base_team_name}."}

    # Now, fetch the team's email from the Teams Info file, accommodating both suffix and no-suffix scenarios
    df_teams = pd.read_excel("Teams-Emails/Teams Info.xlsx", engine='openpyxl')
    if found_file_name:
        # If a specific file was found, extract the team name from the file name, considering possible suffixes
        team_name_with_suffix = found_file_name.replace(".xlsx", "").replace("Teams-Emails/", "")
    else:
        team_name_with_suffix = base_team_name  # Use the base team name directly if no file was found

    # Attempt to match the team name, accommodating spaces removal
    team_email_row = df_teams[df_teams['Team Name'].str.replace(" ", "").str.lower() == team_name_with_suffix.replace(" ", "").lower()]
    
    if not team_email_row.empty:
        team_email = team_email_row['Team Email'].iloc[0]
    else:
        return None,{"error": f"Team {team_name_with_suffix} not found in the Teams Info.xlsx file."}

    details = {
        "assignee_email": assignee_email,
        "assignee_name": assignee_name,
        "team_email": team_email,
        "file_name": found_file_name  # Include the found file name for clarity
    }

    print(f"Team Name: {team_name_with_suffix}")
    print(f"UID: {uid}")
    print(f"Assignee Name: {details['assignee_name']}")
    print(f"Assignee Email: {details['assignee_email']}")
    print(f"Team Email: {details['team_email']}")

    return team_name_with_suffix, details


def get_team_from_excel(issue_key, file_path):
    issue_key = issue_key.strip('()')
    df = pd.read_excel(file_path)
    team_series = df[df['Key'].str.contains(issue_key.split('-')[0], case=False)]['Team']

    print(f"Extracted team(s) for {issue_key}: {team_series.tolist() if team_series.tolist() else 'None'}")

    if not team_series.empty:
        # If the key is  "JO00ECCU", return a list of teams
        if issue_key.split('-')[0] in ["JO00ECCU"]:
            return team_series.tolist()
        return team_series.iloc[0]
    else:
        return None
def mention_person_or_team(browser, text_field, name, possible_teams):
    text_field.send_keys("@" + name.split()[0])
    dropdown = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.XPATH, '/html/body/div[3]/div/div[1]/div/div[2]/ul'))
    )
    items = dropdown.find_elements(By.XPATH, '/html/body/div[3]/div/div[1]/div/div[2]/ul//li')

    print(f"Trying to mention: {name}")
    #print(f"Possible teams for mention: {possible_teams}")

    uid_found = False
    for item in items:
        item_text = item.text.strip().split('(')[0].replace(" ", "").lower()
        if "@" + name.strip().replace(" ", "").lower() in item_text:
            uid_matches = re.findall(r'\((u\d+)\)', item.text, re.IGNORECASE)
            #print(uid_matches)
            if uid_matches :  # Check if we found a UID match
                uid = uid_matches[0]
                print(f"Extracted UID: {uid} from item text: {item.text}")  # Debugging line
                uid_found = True
                for team_name in possible_teams:
                    team_with_suff, details = get_assignee_and_team_details(team_name, uid)  # Use the extracted UID directly
                    if 'error' not in details:
                        try:
                            browser.execute_script("arguments[0].scrollIntoView(true);", item)
                            item.click()
                        except:
                            browser.execute_script("arguments[0].click();", item)
                        return team_with_suff,details


    if not uid_found:
        for item in items:
            item_text = item.text.strip().split('(')[0].replace(" ", "").lower()
            if "@" + name.strip().replace(" ", "").lower() in item_text:
                try:
                    browser.execute_script("arguments[0].scrollIntoView(true);", item)
                    item.click()
                except:
                    browser.execute_script("arguments[0].click();", item)
                return None,None

    return None,None


def write_to_webpage(issue_key, message):
    possible_teams = get_team_from_excel(issue_key, 'T&P/Teams&Projects.xlsx')
    
    if not possible_teams:
        print(f"No team found for issue key: {issue_key}")
        return
    
    if not isinstance(possible_teams, list):
        possible_teams = [possible_teams]

    assignee = browser.find_element(By.CSS_SELECTOR, 'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > ul > li:nth-child(1) > span.ng-star-inserted > label > a').text
    text_field = browser.find_element(By.XPATH, '//*[@id="Comments"]/app-comments/div/div[1]/ckeditor/div/div[2]/div/p')
    
    text_field.send_keys("Dear ")
    correct_team, details = mention_person_or_team(browser, text_field, assignee, possible_teams)
    
    if not correct_team:
        # Clear the comment
        text_field.clear()
        print(f"Correct team not found for assignee {assignee}. Skipping ticket {issue_key}.")
        return None

        
    text_field.send_keys(',\n\n' + message + '\n\n Best Regards,\n')
    # mention_person_or_team(browser, text_field, 'Yazan Albustanji', [])
    text_field.send_keys("\n")
    mention_person_or_team(browser, text_field, 'SupportT1 .', [])
    text_field.send_keys("\n")
    mention_person_or_team(browser, text_field, correct_team, [])

    # Locate and fill the specified input box with the number 1
    input_box = browser.find_element(By.XPATH, '//*[@id="Comments"]/app-comments/div/div[1]/i/input[3]')
    input_box.send_keys('1')

    # Click on the "Post" button
    # post_button = browser.find_element(By.XPATH, '//*[@id="Comments"]/app-comments/div/div[1]/span/button')  # Replace with the correct XPATH or CSS selector
    # post_button.click()

    # private_option = WebDriverWait(browser, 10).until(
    #     EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div[2]/div/div/div/mat-option[2]'))  # Replace with the correct XPATH or CSS selector
    # )
    # private_option.click()


    issue_key = issue_key.strip('()')
    return {
        'Ticket Key': issue_key,
        'Assignee Name': assignee,
        'Assignee Email': details['assignee_email'],
        'Team Name': correct_team,
        'Team Email': details['team_email'],
        'Update Type': '',
        'Action':'' 
    }



wb = openpyxl.load_workbook('output/tickets.xlsx')
ws = wb.active

ticket_links = []

for row in ws.iter_rows(min_row=2, max_row=ws.max_row, min_col=2, max_col=2):
    for cell in row:
        ticket_links.append(cell.value)

# Start a browser session
chrome_options = webdriver.ChromeOptions()
current_directory = os.getcwd()
cookies_path = os.path.join(current_directory, "chrome-cookies")
chrome_options.add_argument(f"--user-data-dir={cookies_path}")


browser = webdriver.Chrome(options=chrome_options)
df_tickets = pd.DataFrame(columns=['Ticket Key', 'Assignee Name', 'Assignee Email', 'Team Name', 'Team Email', 'Update Type','Ticket Link','Action'])

scraped_data = []


wb = openpyxl.load_workbook('output/tickets.xlsx')
ws = wb.active

for link in ticket_links:


    browser.get(link)
    
    
    try:

        WebDriverWait(browser, 25).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="Comments"]/app-comments/div/div[2]/div[1]/div/div/span'))
        )

  
        try:
            date_labels = browser.find_elements(By.XPATH, "/html/body/app-root/app-user-dashboard/body/div[2]/app-tab-ticket/div/div/div/div/app-issue-info/div/div[2]/div/div/div[2]/div[1]/app-details-section/div/div[2]/div[1]/ul")
            
            dueDate = "N/A"
            for label in date_labels:
                if "Due Date:" in label.text:
                    due_date_element = label.find_element(By.XPATH, "./div[3]/div[2]")
                    dueDate = due_date_element.text
                    break
        except NoSuchElementException:
            dueDate = "N/A"
        
        try:
            details_labels = browser.find_elements(By.XPATH, "/html/body/app-root/app-user-dashboard/body/div[2]/app-tab-ticket/div/div/div/div/app-issue-info/div/div[2]/div/div/div[2]/div[1]/app-details-section/div/div[1]/div[2]")
            environment = "N/A" 
            for label in details_labels:  
                if "Environment" in label.text:
                    env_element = label.find_element(By.XPATH, "./div[3]/div[2]/div") 
                    environment = env_element.text  
                    break
        except NoSuchElementException:
            environment = "N/A" 
        
        try:
            labels_section_xpath = "/html/body/app-root/app-user-dashboard/body/div[2]/app-tab-ticket/div/div/div/div/app-issue-info/div/div[2]/div/div/div[2]/div[1]/app-details-section/div/div[1]/div[2]/div[5]/div[2]/div[1]/table/tr/td/div/span"
            labels = browser.find_elements(By.XPATH, labels_section_xpath)
            
            cr_label_present = False
            pattern = re.compile(r'\bCR\b')

            
            for label in labels:
                if pattern.search(label.text):
                    cr_label_present = True
                    break
            
        except NoSuchElementException:
            print("The labels section was not found.")

        browser.implicitly_wait(1)
        wait = WebDriverWait(browser, 10)  # 10 seconds wait time, adjust as necessary

        try:
            assignee = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > ul > li:nth-child(1) > span.ng-star-inserted > label > a'))).text
            reporter = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > ul > li:nth-child(2) > span.ng-star-inserted > label > a'))).text
            first_commenter = wait.until(EC.visibility_of_element_located((By.XPATH, '/html/body/app-root/app-user-dashboard/body/div[2]/app-tab-ticket/div/div/div/div/app-issue-info/div/div[2]/div/div/div[2]/div[5]/app-comments/div/div[2]/div[1]/div/label/a'))).text
            first_comment = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="Comments"]/app-comments/div/div[2]/div[1]/div/div/span'))).text
            ticket_type = wait.until(EC.visibility_of_element_located((By.XPATH, '/html/body/app-root/app-user-dashboard/body/div[2]/app-tab-ticket/div/div/div/div/app-issue-info/div/div[2]/div/div/div[2]/div[1]/app-details-section/div/div[1]/div[2]/div[1]/div[2]/div/div[2]'))).text
            last_updated = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div.divBlock2 > div.dTable2 > ul > div:nth-child(2) > div.right'))).text
            ticket_status = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div:nth-child(1) > div.dTable > div:nth-child(2) > div.right > div'))).text
            issue_key = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div.leftHeader.adjust-height > label > span'))).text
        except TimeoutException as e:
            print("Timed out waiting for page elements to load:", e)

        record = {
            "issue_key":issue_key,
            "dueDate": dueDate,
            "environment": environment,
            "assignee": assignee,
            "first_commenter": first_commenter,
            "first_comment":first_comment,
            "reporter": reporter,
            "ticket_type": ticket_type,
            "last_updated": last_updated,
            "ticket_status": ticket_status,
            "update_status":"",
        }
        scraped_data.append(record)


        
        def count_weekdays(start_date, end_date):
            weekday_count = 0
            current_date = start_date
            while current_date < end_date:
                # Monday to Thursday are counted as weekdays
                if current_date.weekday() < 4:  # 0 is Monday, 3 is Thursday
                    weekday_count += 1
                current_date += timedelta(days=1)
            return weekday_count
        


        date_format = "%b %d, %Y, %I:%M %p"
        date_due_date_format = "%b %d, %Y"
        for record in scraped_data:
            due_date_str = record['dueDate']
            last_updated_date_str = record['last_updated']
            ticket_type = record['ticket_type']
            first_comment = record['first_comment'].lower().strip()  

        message = None

        # Convert last_updated_date_str to a datetime object
        last_updated_date = datetime.strptime(last_updated_date_str, date_format)
        # Calculate weekdays since last update
        weekday_count_since_last_update = count_weekdays(last_updated_date, datetime.now())

        if not cr_label_present:
            if "this is the third reminder" in first_comment:
                due_date_check = due_date_str == "N/A" or datetime.strptime(due_date_str, date_due_date_format) < (datetime.now() - timedelta(days=2))
                if due_date_check:
                    if environment.lower() != "test" and "Ticket" in ticket_type and weekday_count_since_last_update >= 2:
                        record['update_status'] = "First Escalation - Production"
                    elif environment.lower() == "test" or "Service" in ticket_type and weekday_count_since_last_update >= 7:
                        record['update_status'] = "First Escalation - Service"

            elif message == None and ("kindly if there is any update" in first_comment or "post another reminder to the client" in first_comment):
                due_date_check = due_date_str == "N/A" or datetime.strptime(due_date_str, date_due_date_format) < (datetime.now() - timedelta(days=2))
                if due_date_check:
                    if environment.lower() != "test" and "Ticket" in ticket_type and weekday_count_since_last_update >= 2:
                        record['update_status'] = "Second Reminder - Production"
                        message = "Kindly note that this is the second reminder to provide an update."
                    elif environment.lower() == "test" or "Service" in ticket_type and weekday_count_since_last_update >= 7:
                        record['update_status'] = "Second Reminder - Service"
                        message = "Kindly note that this is the second reminder to provide an update."

            elif message == None and "this is the second reminder" in first_comment:
                due_date_check = due_date_str == "N/A" or datetime.strptime(due_date_str, date_due_date_format) < (datetime.now() - timedelta(days=2))
                if due_date_check:
                    if environment.lower() != "test" and "Ticket" in ticket_type and weekday_count_since_last_update >= 2:
                        record['update_status'] = "Third Reminder - Production"
                        message = "Kindly note that this is the third reminder to provide an update."
                    elif environment.lower() == "test" or "Service" in ticket_type and weekday_count_since_last_update >= 7:
                        record['update_status'] = "Third Reminder - Service"
                        message = "Kindly note that this is the third reminder to provide an update."

            elif message == None:
                due_date_check = due_date_str == "N/A" or datetime.strptime(due_date_str, date_due_date_format) < (datetime.now() - timedelta(days=2))
                if due_date_check:
                    if environment.lower() != "test" and "Ticket" in ticket_type and weekday_count_since_last_update >= 2:
                        record['update_status'] = "Short reminder - Production"
                        message = "Kindly if there is any update please provide it on this ticket."
                    elif environment.lower() == "test" or "Service" in ticket_type and weekday_count_since_last_update >= 7:
                        record['update_status'] = "Short reminder - Service"
                        message = "Kindly if there is any update please provide it on this ticket."
        else:
            print("This is a CR Ticket")


        if message:
            ticket_details = write_to_webpage(issue_key, message)
            if ticket_details:
                
                ticket_link = browser.current_url  

                ticket_details['Ticket Link'] = ticket_link
                ticket_details['Update Type'] = record['update_status']

                if "Second Reminder" in record['update_status']:
                    ticket_details['Action'] = f"Second Reminder for {assignee.split()[0]} to provide updates"
                elif "Short reminder" in record['update_status']:
                    ticket_details['Action'] = f"Asked {assignee.split()[0]} to provide updates"
                elif "Third Reminder" in record['update_status']:
                    ticket_details['Action'] = f"Third Reminder for {assignee.split()[0]} to provide updates"
 
                
                df_tickets = pd.concat([df_tickets, pd.DataFrame([ticket_details])], ignore_index=True)

        
        
        # print(f"Is due date in the past or N/A? {due_date_str == 'N/A' or datetime.strptime(due_date_str, date_format) < datetime.now()}")
        # print(f"Is last updated more than 7 days ago? {datetime.strptime(last_updated_date_str, date_format) <= datetime.now() - timedelta(days=7)}")
        # print(f"Is ticket type Service? {ticket_type == 'Service'}")
        
        print(f"Scraped data from ticket {issue_key},{dueDate}, {environment}, {assignee},{reporter},{first_commenter},{last_updated},{ticket_type},{ticket_status},{ticket_type}",record["update_status"])
        print("----------------------------------------------------------------------------------------------")
        
    except NoSuchElementException as e:
        print(f"Failed to scrape data from ticket {link}: {e}")
    except TimeoutException as e:
        print(f"Timed out waiting for element to load on ticket {link}: {e}")


ws = wb.active

df_tickets.to_excel('output/sending_emails.xlsx', index=False, engine='openpyxl')


new_headers = ["Due Date","Environment","Last Update","Ticket Type","Assignee", "First Commenter", "Reporter","Update Status"]
for col_num, header in enumerate(new_headers, 3):  
    ws.cell(row=1, column=col_num, value=header)

for row_num, record in enumerate(scraped_data, 2):  # Starting from 2nd row
    ws.cell(row=row_num, column=3, value=record["dueDate"])
    ws.cell(row=row_num, column=4, value=record["environment"])  
    ws.cell(row=row_num, column=5, value=record["last_updated"])  
    ws.cell(row=row_num, column=6, value=record["ticket_type"])  
    ws.cell(row=row_num, column=7, value=record["assignee"]) 
    ws.cell(row=row_num, column=8, value=record["first_commenter"])
    ws.cell(row=row_num, column=9, value=record["reporter"])
    ws.cell(row=row_num, column=10, value=record["update_status"])
# Save the changes
wb.save('output/tickets.xlsx')

browser.quit()








