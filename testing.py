from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from datetime import datetime, timedelta
import smtplib
from email.message import EmailMessage
import pandas as pd
import openpyxl
import re
import time


def get_assignee_and_team_details(team_name, uid):
   
    if isinstance(team_name, list):
        team_name = team_name[0]

    team_file_path = f"Teams-Emails/{team_name}.xlsx"
    
    try:
        df_team = pd.read_excel(team_file_path, engine='openpyxl')
    except FileNotFoundError:
        return {"error": f"File {team_file_path} not found."}
    
    matching_rows = df_team[df_team['UID'].str.replace(" ", "").str.lower() == uid.replace(" ", "").lower()]
  
    
    if not matching_rows.empty:
        assignee_email = matching_rows['Email'].iloc[0]
        assignee_name = matching_rows['Team Members'].iloc[0]
    else:
        return {"error": f"UID {uid} not found for team {team_name}."}
    
    df_teams = pd.read_excel("Teams-Emails/Teams Info.xlsx", engine='openpyxl')
    team_email_row = df_teams[df_teams['Team Name'].str.replace(" ", "") == team_name.replace(" ", "")]

    if not team_email_row.empty:
        team_email = team_email_row['Team Email'].iloc[0]
    else:
        return {"error": f"Team {team_name} not found in the Teams-Info.xlsx file."}
    
    details = {
        "assignee_email": assignee_email,
        "assignee_name": assignee_name,
        "team_email": team_email
    }
    print(f"Team Name: {team_name}")
    print(f"UID: {uid}")
    print(f"Assignee Name: {details['assignee_name']}")
    print(f"Assignee Email: {details['assignee_email']}")
    print(f"Team Email: {details['team_email']}")

    return details
    

def get_team_from_excel(issue_key, file_path):
    issue_key = issue_key.strip('()')
    df = pd.read_excel(file_path)
    team_series = df[df['Key'].str.contains(issue_key.split('-')[0], case=False)]['Team']

    print(f"Extracted team(s) for {issue_key}: {team_series.tolist() if team_series.tolist() else 'None'}")

    if not team_series.empty:
        # If the key is "JO07HBTF" or "JO00ECCU", return a list of teams
        if issue_key.split('-')[0] in ["JO07HBTF", "JO00ECCU"]:
            return team_series.tolist()
        return team_series.iloc[0]
    else:
        return None
def mention_person_or_team(browser, text_field, name, possible_teams):
    text_field.send_keys("@" + name.split()[0])
    dropdown = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.XPATH, '/html/body/div[3]/div/div[1]/div/div[2]/ul'))
    )
    items = dropdown.find_elements(By.XPATH, '/html/body/div[3]/div/div[1]/div/div[2]/ul//li')

    print(f"Trying to mention: {name}")
    #print(f"Possible teams for mention: {possible_teams}")

    uid_found = False
    for item in items:
        item_text = item.text.strip().split('(')[0].replace(" ", "").lower()
        if "@" + name.strip().replace(" ", "").lower() in item_text:
            uid_matches = re.findall(r'\((u\d+)\)', item.text, re.IGNORECASE)
            #print(uid_matches)
            if uid_matches:  # Check if we found a UID match
                uid = uid_matches[0]
                print(f"Extracted UID: {uid} from item text: {item.text}")  # Debugging line
                uid_found = True
                for team_name in possible_teams:
                    details = get_assignee_and_team_details(team_name, uid)  # Use the extracted UID directly
                    if 'error' not in details:
                        try:
                            browser.execute_script("arguments[0].scrollIntoView(true);", item)
                            item.click()
                        except:
                            browser.execute_script("arguments[0].click();", item)
                        return team_name,details


    if not uid_found:
        for item in items:
            item_text = item.text.strip().split('(')[0].replace(" ", "").lower()
            if "@" + name.strip().replace(" ", "").lower() in item_text:
                try:
                    browser.execute_script("arguments[0].scrollIntoView(true);", item)
                    item.click()
                except:
                    browser.execute_script("arguments[0].click();", item)
                return None,None

    return None,None


def write_to_webpage(issue_key, message):
    possible_teams = get_team_from_excel(issue_key, 'T&P/Teams&Projects.xlsx')
    
    if not possible_teams:
        print(f"No team found for issue key: {issue_key}")
        return
    
    if not isinstance(possible_teams, list):
        possible_teams = [possible_teams]

    assignee = browser.find_element(By.CSS_SELECTOR, 'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > ul > li:nth-child(1) > span.ng-star-inserted > label > a').text
    text_field = browser.find_element(By.XPATH, '//*[@id="Comments"]/app-comments/div/div[1]/ckeditor/div/div[2]/div/p')
    
    text_field.send_keys("Dear ")
    correct_team, details = mention_person_or_team(browser, text_field, assignee, possible_teams)
    
    if not correct_team:
        # Clear the comment
        text_field.clear()
        print(f"Correct team not found for assignee {assignee}. Skipping ticket {issue_key}.")
        return None

        
    text_field.send_keys(',\n\n' + message + '\n\n Best Regards,\n')
    mention_person_or_team(browser, text_field, 'SupportT1 .', [])
    text_field.send_keys("\n")
    mention_person_or_team(browser, text_field, correct_team, [])
    # time.sleep(3)



    # Click on the "Post" button
    # post_button = browser.find_element(By.XPATH, '//*[@id="Comments"]/app-comments/div/div[1]/span/button')  # Replace with the correct XPATH or CSS selector
    # post_button.click()

    # private_option = WebDriverWait(browser, 10).until(
    #     EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div[2]/div/div/div/mat-option[2]'))  # Replace with the correct XPATH or CSS selector
    # )
    # private_option.click()



    issue_key = issue_key.strip('()')
    return {
        'Ticket Key': issue_key,
        'Assignee Name': assignee,
        'Assignee Email': details['assignee_email'],
        'Team Name': correct_team,
        'Team Email': details['team_email'],
        'Update Type': ''  
    }




chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--user-data-dir=/home/motasem/Desktop/Work/Automation/Follow_Up_Script/chrome-cookies")


browser = webdriver.Chrome(options=chrome_options)
df_tickets = pd.DataFrame(columns=['Ticket Key', 'Assignee Name', 'Assignee Email', 'Team Name', 'Team Email', 'Update Type'])

scraped_data = []

for i in range(5):
    
    ticket_link = "https://support.progressoft.com/dash/ticket/1017339"

    browser.get(ticket_link)

        # Scrape data from the ticket page
    try:

        WebDriverWait(browser, 25).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="Comments"]/app-comments/div/div[2]/div[1]/div/div/span'))
        )


        try:
            # Find all the date labels
            date_labels = browser.find_elements(By.XPATH, "/html/body/app-root/app-user-dashboard/body/div[2]/app-tab-ticket/div/div/div/div/app-issue-info/div/div[2]/div/div/div[2]/div[1]/app-details-section/div/div[1]/div[3]/div[2]/ul/li")
            
            dueDate = "N/A"
            for label in date_labels:
                if "Due Date:" in label.text:
                    due_date_element = label.find_element(By.XPATH, "./i")
                    dueDate = due_date_element.text
                    break
        except NoSuchElementException:
            dueDate = "N/A"


        assignee = browser.find_element(By.CSS_SELECTOR, 'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > ul > li:nth-child(1) > span.ng-star-inserted > label > a').text
        reporter = browser.find_element(By.CSS_SELECTOR,'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > ul > li:nth-child(2) > span.ng-star-inserted > label > a').text
        first_commenter = browser.find_element(By.XPATH, '/html/body/app-root/app-user-dashboard/body/div[2]/app-tab-ticket/div/div/div/div/app-issue-info/div/div[2]/div/div/div[2]/div[5]/app-comments/div/div[2]/div[1]/div/label/a').text
        first_comment = browser.find_element(By.XPATH, '//*[@id="Comments"]/app-comments/div/div[2]/div[1]/div/div/span').text
        ticket_type = browser.find_element(By.XPATH,'/html/body/app-root/app-user-dashboard/body/div[2]/app-tab-ticket/div/div/div/div/app-issue-info/div/div[2]/div/div/div[2]/div[1]/app-details-section/div/div[1]/div[2]/div[1]/div[2]/div/div[2]').text
        last_updated = browser.find_element(By.CSS_SELECTOR, 'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(2) > ul > li:nth-child(2) > i').text
        ticket_status = browser.find_element(By.CSS_SELECTOR,'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div:nth-child(1) > div.dTable > div:nth-child(2) > div.right > div').text
        issue_key = browser.find_element(By.CSS_SELECTOR, 'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div.leftHeader.adjust-height > label > span').text


        record = {
            "issue_key":issue_key,
            "dueDate": dueDate,
            "assignee": assignee,
            "first_commenter": first_commenter,
            "first_comment":first_comment,
            "reporter": reporter,
            "ticket_type": ticket_type,
            "last_updated": last_updated,
            "ticket_status": ticket_status,
            "update_status":"",
        }
        scraped_data.append(record)



        date_format = "%b %d, %Y, %I:%M %p"
        for record in scraped_data:
            due_date_str = record['dueDate']
            last_updated_date_str = record['last_updated']
            ticket_type = record['ticket_type']
            first_comment = record['first_comment'].lower().strip()  

        record['update_status'] = "Short reminder - Service"
        # # Check for First Escalation
        # if "kindly note that this is the third reminder" in first_comment:
        #     if due_date_str == "N/A" or datetime.strptime(due_date_str, date_format) < datetime.now():
        #         if "Ticket" in ticket_type and datetime.strptime(last_updated_date_str, date_format) <= datetime.now() - timedelta(days=2):
        #             record['update_status'] = "First Escalation - Production" 
        #             # message = "Kindly note that this is the first escalation on this ticket after three reminders with no response."
        #         elif "Service" in ticket_type and datetime.strptime(last_updated_date_str, date_format) <= datetime.now() - timedelta(days=7):
        #             record['update_status'] = "First Escalation - Service"
        #             # message = "Kindly note that this is the first escalation on this ticket after three reminders with no response."


        # # Check for Second Reminder
        # if ("kindly if there is any update" in first_comment) or ("short reminder" in first_comment.lower()) or ("reminder to the client" in first_comment.lower()):
        #     if due_date_str == "N/A" or datetime.strptime(due_date_str, date_format) < datetime.now():
        #         if "Ticket" in ticket_type and datetime.strptime(last_updated_date_str, date_format) <= datetime.now() - timedelta(days=2):
        #             record['update_status'] = "Second Reminder - Production"
        #             message = "Kindly note that this is the second reminder to provide an update."
        #         elif "Service" in ticket_type and datetime.strptime(last_updated_date_str, date_format) <= datetime.now() - timedelta(days=7):
        #             record['update_status'] = "Second Reminder - Service"
        #             message = "Kindly note that this is the second reminder to provide an update."


        # # Check for Third Reminder
        # if "kindly note that this is the second reminder" in first_comment:
        #     if due_date_str == "N/A" or datetime.strptime(due_date_str, date_format) < datetime.now():
        #         if "Ticket" in ticket_type and datetime.strptime(last_updated_date_str, date_format) <= datetime.now() - timedelta(days=2):
        #             record['update_status'] = "Third Reminder - Production"
        #             message = "Kindly note that this is the third reminder to provide an update."

        #         elif "Service" in ticket_type and datetime.strptime(last_updated_date_str, date_format) <= datetime.now() - timedelta(days=7):
        #             record['update_status'] = "Third Reminder - Service"
        #             message = "Kindly note that this is the third reminder to provide an update."


        # # Existing conditions for "Short reminder - Production"
        # if due_date_str == "N/A" or datetime.strptime(due_date_str, date_format) < datetime.now():
        #     if "Ticket" in ticket_type and datetime.strptime(last_updated_date_str, date_format) <= datetime.now() - timedelta(days=2):
        #         if not record['update_status']:  # Only update if no previous status was set
        #             record['update_status'] = "Short reminder - Production"
        #             message = "Kindly if there is any update please provide it on this ticket."


        # # Existing conditions for "Short reminder - Service"
        # if due_date_str == "N/A" or datetime.strptime(due_date_str, date_format) < datetime.now():
        #     if "Service" in ticket_type and datetime.strptime(last_updated_date_str, date_format) <= datetime.now() - timedelta(days=7):
        #         if not record['update_status']:  # Only update if no previous status was set
        #             record['update_status'] = "Short reminder - Service"
        #             message = "Kindly if there is any update please provide it on this ticket."

        message = "Kindly if there is any update please provide it on this ticket."
        write_to_webpage(issue_key, "Kindly if there is any update please provide it on this ticket.")
        


        # print(f"Is due date in the past or N/A? {due_date_str == 'N/A' or datetime.strptime(due_date_str, date_format) < datetime.now()}")
        # print(f"Is last updated more than 7 days ago? {datetime.strptime(last_updated_date_str, date_format) <= datetime.now() - timedelta(days=7)}")
        # print(f"Is ticket type Service? {ticket_type == 'Service'}")

        print(f"Scraped data from ticket {dueDate}, {assignee}, {first_commenter}, {reporter},{last_updated},{ticket_type},{ticket_status},{ticket_type}",record["update_status"])
        print("----------------------------------------------------------------------------------------------")

    except NoSuchElementException as e:
        print(f"Failed to scrape data from ticket {ticket_link}: {e}")

    
browser.quit()
