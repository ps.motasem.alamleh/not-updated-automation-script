import subprocess

scripts = ["links_scrape.py", "tickets_scrape_core_updated.py", "sending_emails.py"]

for script in scripts:
    print(f"Running {script}...")
    process = subprocess.Popen(["python3", script], stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    # Monitor stdout
    while True:
        output = process.stdout.readline()
        if output:
            print(output.strip())
        elif process.poll() is not None:
            break

    # Check if the process exited with an error
    err = process.stderr.read()
    if err:
        print(f"Error executing {script}:\n{err.strip()}")
        break  # Stop executing the rest of the scripts if an error occurred
    
    if process.returncode == 0:
        print(f"{script} completed successfully.\n{'-'*40}\n")
    else:
        print(f"{script} failed with return code {process.returncode}.\n{'-'*40}\n")
        break  # Exit the loop if a script fails

if process.returncode == 0:
    print("All scripts executed successfully.")
else:
    print("Execution halted due to an error.")
