        if not cr_label_present:
            # Check for First Escalation
            if "this is the third reminder" in first_comment:
                if due_date_str == "N/A" or datetime.strptime(due_date_str, date_due_date_format) < (datetime.now() - timedelta(days=2)):
                    if environment.lower() != "test" and "Ticket" in ticket_type and weekday_count_since_last_update >= 2:
                        record['update_status'] = "First Escalation - Production" 
                        # message = "Kindly note that this is the first escalation on this ticket after three reminders with no response."
                    elif environment.lower() == "test" or "Service" in ticket_type and weekday_count_since_last_update >= 7:
                        record['update_status'] = "First Escalation - Service"
                        # message = "Kindly note that this is the first escalation on this ticket after three reminders with no response."
            
            
            # Check for Second Reminder
            if message == None and "kindly if there is any update" in first_comment or "post another reminder to the client" in first_comment :
                if due_date_str == "N/A" or datetime.strptime(due_date_str, date_due_date_format) < (datetime.now() - timedelta(days=2)):
                    if environment.lower() != "test" and "Ticket" in ticket_type and weekday_count_since_last_update >= 2:
                        record['update_status'] = "Second Reminder - Production"
                        message = "Kindly note that this is the second reminder to provide an update."
                    elif environment.lower() == "test" or "Service" in ticket_type and weekday_count_since_last_update >= 7:
                        record['update_status'] = "Second Reminder - Service"
                        message = "Kindly note that this is the second reminder to provide an update."


            # Check for Third Reminder
            if message == None and "this is the second reminder" in first_comment:
                if due_date_str == "N/A" or datetime.strptime(due_date_str, date_due_date_format) < (datetime.now() - timedelta(days=2)):
                    if environment.lower() != "test" and "Ticket" in ticket_type and weekday_count_since_last_update >= 2:
                        record['update_status'] = "Third Reminder - Production"
                        message = "Kindly note that this is the third reminder to provide an update."

                    elif environment.lower() == "test" or "Service" in ticket_type and weekday_count_since_last_update >= 7:
                        record['update_status'] = "Third Reminder - Service"
                        message = "Kindly note that this is the third reminder to provide an update."


            # Check for Short reminder
            if message == None:
                if due_date_str == "N/A" or datetime.strptime(due_date_str, date_due_date_format) < (datetime.now() - timedelta(days=2)):
                    if environment.lower() != "test" and "Ticket" in ticket_type and weekday_count_since_last_update >= 2:
                        record['update_status'] = "Short reminder - Production"
                        message = "Kindly if there is any update please provide it on this ticket."

                    elif environment.lower() == "test" or "Service" in ticket_type and weekday_count_since_last_update >= 7:
                        record['update_status'] = "Short reminder - Service"
                        message = "Kindly if there is any update please provide it on this ticket."                    





/////////////////////////////////////////


        assignee = browser.find_element(By.CSS_SELECTOR, 'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > ul > li:nth-child(1) > span.ng-star-inserted > label > a').text
        reporter = browser.find_element(By.CSS_SELECTOR,'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > ul > li:nth-child(2) > span.ng-star-inserted > label > a').text
        first_commenter = browser.find_element(By.XPATH, '/html/body/app-root/app-user-dashboard/body/div[2]/app-tab-ticket/div/div/div/div/app-issue-info/div/div[2]/div/div/div[2]/div[5]/app-comments/div/div[2]/div[1]/div/label/a').text
        first_comment = browser.find_element(By.XPATH, '//*[@id="Comments"]/app-comments/div/div[2]/div[1]/div/div/span').text
        ticket_type = browser.find_element(By.XPATH,'/html/body/app-root/app-user-dashboard/body/div[2]/app-tab-ticket/div/div/div/div/app-issue-info/div/div[2]/div/div/div[2]/div[1]/app-details-section/div/div[1]/div[2]/div[1]/div[2]/div/div[2]').text
        last_updated = browser.find_element(By.CSS_SELECTOR, 'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div.divBlock2 > div.dTable2 > ul > div:nth-child(2) > div.right').text
        ticket_status = browser.find_element(By.CSS_SELECTOR,'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div:nth-child(2) > div:nth-child(1) > app-details-section > div > div:nth-child(1) > div.dTable > div:nth-child(2) > div.right > div').text
        issue_key = browser.find_element(By.CSS_SELECTOR, 'body > div.index > app-tab-ticket > div > div > div > div > app-issue-info > div > div.tabInfo.ng-star-inserted > div > div > div.leftHeader.adjust-height > label > span').text
